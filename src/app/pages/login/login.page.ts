import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { IonSlides, NavController } from '@ionic/angular';
import { UiserviceService } from 'src/app/services/uiservice.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  avatars = [
    {
      img: 'av-1.png',
      seleccionado: true
    },
    {
      img: 'av-2.png',
      seleccionado: false
    },
    {
      img: 'av-3.png',
      seleccionado: false
    },
    {
      img: 'av-4.png',
      seleccionado: false
    },
    {
      img: 'av-5.png',
      seleccionado: false
    },
    {
      img: 'av-6.png',
      seleccionado: false
    },
    {
      img: 'av-7.png',
      seleccionado: false
    },
    {
      img: 'av-8.png',
      seleccionado: false
    },
  ];


  //validar formulario
  login( flogin: NgForm ){
    console.log ( flogin.valid ); 
    if ( flogin.invalid ){ 
      this.uiService.alertaInformativa('Verifique su información los campos no coinciden');
      return;
    }else{
      this.navCtrl.navigateRoot('/home', { animated: true });
    }
  }

  registro( fRegistro: NgForm ){
    console.log ( fRegistro.valid );
  }

  @ViewChild('slidePrincipal') slides: IonSlides;

  

  loginUser = {
    name: '',
    documento: '',
    edad: ''
  };

  registerUser = {
    email: '',
    password: '',
    nombre: '',
    avatar: 'av-1.png'
  };
  

  constructor( private navCtrl: NavController, 
                private uiService: UiserviceService
                 ) {}

  ngOnInit() {
    this.slides.lockSwipes( true );
  }


  mostrarRegistro() {
    this.slides.lockSwipes(false);
    this.slides.slideTo(0);
    this.slides.lockSwipes(true);
  }

  mostrarLogin() {
    this.slides.lockSwipes(false);
    this.slides.slideTo(1);
    this.slides.lockSwipes(true);
  }

  seleccionAvatar( avatar ){
    this.avatars.forEach( av => av.seleccionado = false); 
    avatar.seleccionado = true;
  }
}
